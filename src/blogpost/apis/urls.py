from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from blogpost.apis.views import (
    EmailVerify,
    ResetPasswordEmailSendView,
    ResetPasswordView,
    UserChangePasswordView,
    UserDetailView,
    UserLoginView,
    UserProfileView,
    UserSignupView,
)

urlpatterns = [
    path("user-signup/", UserSignupView.as_view(), name="user-signup"),
    path("user-detail/<int:id>/", UserDetailView.as_view(), name="user-detail"),
    path("user-login/", UserLoginView.as_view(), name="user-login"),
    path("refresh-token/", TokenRefreshView.as_view(), name="token-refresh"),
    path("email-verify/", EmailVerify.as_view(), name="email-verify"),
    path(
        "reset-password-send-email/",
        ResetPasswordEmailSendView.as_view(),
        name="reset-password-send-email",
    ),
    path("reset-password/", ResetPasswordView.as_view(), name="reset-password"),
    path("user-profile/", UserProfileView.as_view(), name="user-profile"),
    path("change-password/", UserChangePasswordView.as_view(), name="change-password"),
]
