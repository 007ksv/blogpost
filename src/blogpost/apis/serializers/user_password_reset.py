import jwt
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from jwt.exceptions import DecodeError, ExpiredSignatureError
from rest_framework import serializers
from rest_framework.exceptions import NotFound

from blogpost.apis.models import User
from blogpost.apis.notification.reset_password import ResetPasswordNotification


class ResetPasswordSerializer(serializers.Serializer):
    password = serializers.CharField()
    token = serializers.CharField()

    def validate(self, attrs):
        token = attrs.get("token")
        try:
            payload = jwt.decode(token, settings.SECRET_KEY)
            user: User = User.objects.get(email=payload.get("username"))

        except DecodeError:
            raise serializers.ValidationError("Invalid token")
        except ExpiredSignatureError:
            raise serializers.ValidationError("Token expired")

        attrs["user"] = user
        return attrs

    def save(self, **kwargs):
        user: User = self.validated_data.get("user")
        raw_password = self.validated_data.get("password")
        update_fields = user.set_password(raw_password=raw_password)
        user.save(update_fields=update_fields)


class ResetPasswordEmailSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate(self, attrs):
        email = attrs.get("email").strip().lower()
        try:
            # user must be active
            user = User.objects.get(email=email, is_active=True)
        except ObjectDoesNotExist:
            raise NotFound("User with this email not exists")
        if not user.email_verified:
            raise serializers.ValidationError(f"Email {email} id not verified")
        attrs["user"] = user
        return attrs

    def save(self, **kwargs):
        user: User = self.validated_data.get("user")
        request = self.context["request"]
        url = user.get_reset_password_link(host=request.META.get("HTTP_HOST"))
        context = {
            "user": user,
            "url": url,
        }
        notification = ResetPasswordNotification(context=context)
        notification.send_mail(to=[user.email])
