from rest_framework.serializers import ModelSerializer

from blogpost.apis.models import User


class UserSignupSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = (
            "email",
            "password",
        )


class UserDetailSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "uuid",
            "email",
            "date_joined",
        ]


class UserProfileSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = (
            "first_name",
            "last_name",
            "username",
            "email",
            "email_verified",
            "uuid",
            "date_joined",
        )
        read_only_fields = (
            "username",
            "email",
            "email_verified",
            "uuid",
            "date_joined",
        )
