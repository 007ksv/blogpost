import datetime
import uuid

import jwt
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.shortcuts import reverse
from django.utils import timezone


class User(AbstractUser):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4)
    email = models.EmailField(unique=True)
    email_verified = models.BooleanField(default=False)
    last_password_change = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

    def __str__(self):
        return f"User(id={self.id}, email={self.email})"

    @property
    def name(self):
        return f"{self.first_name} {self.last_name}"

    def save(self, *args, **kwargs):
        if not self.id and self.password:
            self.set_password(self.password)
        if self.email:
            self.email = self.email.lower().strip()
        if not self.username:
            self.username = self.email
        if not self.is_superuser and self.username != self.email:
            raise Exception("Username should always be same as email")

        return super().save(*args, **kwargs)

    def get_display_name(self):
        return self.name or self.username or self.email

    def get_email_verify_link(self, host):
        payload = {
            "username": self.email,
            "type": "bearer",
            "exp": datetime.datetime.utcnow()
            + datetime.timedelta(hours=settings.USER_EMAIL_VERIFY_LINK_VALIDITY_HOURS),
            "used_for": "user_email_verification",
        }
        token = jwt.encode(payload, key=settings.SECRET_KEY)
        path = reverse("email-verify")
        url = f"http://{host}{path}?token={token.decode()}"
        return url

    def get_reset_password_link(self, host):
        payload = {
            "username": self.email,
            "type": "bearer",
            "exp": datetime.datetime.utcnow()
            + datetime.timedelta(
                hours=settings.USER_RESET_PASSWORD_VERIFY_LINK_VALIDITY_HOURS
            ),
            "used_for": "user_password_reset",
        }
        token = jwt.encode(payload, key=settings.SECRET_KEY)
        path = reverse("reset-password")
        url = f"http://{host}{path}?token={token.decode()}"
        return url

    def set_password(self, raw_password):
        super().set_password(raw_password)
        self.last_password_change = timezone.now()
        update_fields = [
            "password",
            "last_password_change",
        ]
        return update_fields
