from rest_framework.generics import RetrieveUpdateAPIView

from blogpost.apis.models import User
from blogpost.apis.serializers.user import UserProfileSerializer
from blogpost.apis.utils.response_handler import (
    create_response,
    safe_view_request_handler,
)


class UserProfileView(RetrieveUpdateAPIView):
    serializer_class = UserProfileSerializer
    queryset = User.objects

    def get_object(self):
        return self.request.user

    # @safe_view_request_handler()
    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        response.data = create_response(True, item=response.data)
        return response

    @safe_view_request_handler()
    def update(self, request, *args, **kwargs):
        kwargs["partial"] = True
        response = super().update(request, *args, **kwargs)
        response.data = create_response(True, data=response.data)
        return response
