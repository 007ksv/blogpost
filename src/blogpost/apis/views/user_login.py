from django.contrib.auth import authenticate
from rest_framework import serializers, status
from rest_framework.exceptions import AuthenticationFailed, ValidationError
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken

from blogpost.apis.mixins.views import NoPermissionNoAuthenticationView
from blogpost.apis.utils.response_handler import (
    create_response,
    safe_view_request_handler,
)


class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()


class UserLoginView(NoPermissionNoAuthenticationView):
    serializer_class = UserLoginSerializer

    @safe_view_request_handler()
    def post(self, request):
        user = self.get_user(request=request)
        if not user.email_verified:
            raise ValidationError("User's email is not verified")
        refresh_token = RefreshToken.for_user(user)
        access_token = refresh_token.access_token
        response_data = {
            "access": str(access_token),
            "refresh": str(refresh_token),
        }
        return Response(
            create_response(True, data=response_data), status=status.HTTP_200_OK
        )

    @staticmethod
    def get_user(request):
        data = request.data
        serializer = UserLoginSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        email = serializer.validated_data.get("email")
        password = serializer.validated_data.get("password")
        authentication_kwargs = {
            "request": request,
            "username": email,
            "password": password,
        }
        user = authenticate(**authentication_kwargs)
        if not user:
            raise AuthenticationFailed("Invalid user's credentials")
        return user
