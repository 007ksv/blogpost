from rest_framework import status
from rest_framework.response import Response

from blogpost.apis.mixins.views import NoPermissionNoAuthenticationView
from blogpost.apis.notification.email_verify import (
    EmailVerificationNotification,
)
from blogpost.apis.serializers import UserSignupSerializer
from blogpost.apis.utils import create_response, safe_view_request_handler


class UserSignupView(NoPermissionNoAuthenticationView):
    serializer_class = UserSignupSerializer

    @safe_view_request_handler()
    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        context = {
            "user": user,
            "url": user.get_email_verify_link(host=request.META.get("HTTP_HOST")),
        }
        notification = EmailVerificationNotification(context=context)
        notification.send_mail(to=[user.email])
        return Response(
            create_response(
                True,
                data={
                    "message": "User created successfully, please check your mailbox to verify your email"
                },
            ),
            status=status.HTTP_201_CREATED,
        )
