import jwt
from django.conf import settings
from jwt.exceptions import DecodeError, ExpiredSignatureError
from rest_framework import serializers
from rest_framework.response import Response

from blogpost.apis.mixins.views import NoPermissionNoAuthenticationView
from blogpost.apis.models import User
from blogpost.apis.utils.response_handler import (
    create_response,
    safe_view_request_handler,
)


class EmailVerifyTokenSerializer(serializers.Serializer):
    token = serializers.CharField()

    def validate(self, attrs):
        token = attrs.get("token")
        try:
            payload = jwt.decode(token, settings.SECRET_KEY)
        except DecodeError:
            raise serializers.ValidationError("Invalid token")
        except ExpiredSignatureError:
            raise serializers.ValidationError("Token expired")
        user: User = User.objects.get(email=payload.get("username"))
        attrs["user"] = user
        return attrs

    def save(self, **kwargs):
        user: User = self.validated_data.get("user")
        if not user.email_verified:
            user.email_verified = True
            user.save(update_fields=["email_verified"])


class EmailVerify(NoPermissionNoAuthenticationView):
    @safe_view_request_handler()
    def get(self, request):
        data = request.query_params
        serializer = EmailVerifyTokenSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            create_response(True, data={"detail": "Email Verified Succssfully"})
        )
