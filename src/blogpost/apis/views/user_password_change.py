from rest_framework import serializers, status
from rest_framework.generics import UpdateAPIView
from rest_framework.response import Response

from blogpost.apis.models import User
from blogpost.apis.serializers.user import UserProfileSerializer
from blogpost.apis.utils.response_handler import (
    create_response,
    safe_view_request_handler,
)


class UserChangePasswordSerializer(serializers.Serializer):
    curr_password = serializers.CharField()
    new_password = serializers.CharField()

    def validate(self, attrs):
        curr_password = attrs.get("curr_password")
        user: User = self.instance
        is_password_correct = user.check_password(curr_password)
        if not is_password_correct:
            raise serializers.ValidationError("Current password is incorrect")
        return attrs

    def update(self, instance, validated_data):
        user: User = instance
        new_password = validated_data.get("new_password")
        update_fields = user.set_password(new_password)
        user.save(update_fields=update_fields)
        return user

    def to_representation(self, instance):
        return UserProfileSerializer(instance).data


class UserChangePasswordView(UpdateAPIView):
    serializer_class = UserChangePasswordSerializer
    queryset = User.objects

    def get_object(self):
        return self.request.user

    @safe_view_request_handler()
    def update(self, request, *args, **kwargs):
        super().update(request, *args, **kwargs)
        return Response(
            create_response(True, data={"message": "Password changed successfully"}),
            status=status.HTTP_200_OK,
        )
