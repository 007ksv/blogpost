from rest_framework import status
from rest_framework.response import Response

from blogpost.apis.mixins.views import NoPermissionNoAuthenticationView
from blogpost.apis.serializers import (
    ResetPasswordEmailSerializer,
    ResetPasswordSerializer,
)
from blogpost.apis.utils.response_handler import (
    create_response,
    safe_view_request_handler,
)


class ResetPasswordEmailSendView(NoPermissionNoAuthenticationView):
    """
    send reset password email to user which contain token so that they can reset their password
    """

    serializer_class = ResetPasswordEmailSerializer

    @safe_view_request_handler()
    def post(self, request):
        serializer: ResetPasswordEmailSerializer = self.get_serializer(
            data=request.data
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            create_response(
                True,
                data={
                    "message": "Reset password mail has been send successfully, please check your email"
                },
            ),
            status=status.HTTP_200_OK,
        )


class ResetPasswordView(NoPermissionNoAuthenticationView):

    serializer_class = ResetPasswordSerializer

    @safe_view_request_handler()
    def post(self, request):
        serializer: ResetPasswordSerializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            create_response(True, data={"message": "Password reset successfully"}),
            status=status.HTTP_200_OK,
        )
