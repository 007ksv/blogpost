from rest_framework.generics import GenericAPIView
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.response import Response

from blogpost.apis.models import User
from blogpost.apis.serializers import UserDetailSerializer
from blogpost.apis.utils import create_response, safe_view_request_handler


class UserDetailView(RetrieveModelMixin, GenericAPIView):
    serializer_class = UserDetailSerializer
    queryset = User.objects
    lookup_field = "id"

    @safe_view_request_handler()
    def get(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return Response(create_response(True, data=response.data))
