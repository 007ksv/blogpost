from blogpost.notification.base import BaseNotification


class ResetPasswordNotification(BaseNotification):
    email_subject_template = "Reset Your Password"
    email_body_template = "emails/reset_password.txt"
