from blogpost.notification.base import BaseNotification


class EmailVerificationNotification(BaseNotification):
    email_subject_template = "Verify your email"
    email_body_template = "emails/email_verify.txt"
