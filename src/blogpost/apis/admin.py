from django.contrib import admin
from django.contrib.admin import ModelAdmin

from blogpost.apis.models import User


@admin.register(User)
class UserAdmin(ModelAdmin):
    exclude = ("password",)
    search_fields = ("first_name", "last_name", "username", "email")

    filter_horizontal = ("groups",)

    readonly_fields = (
        "date_joined",
        "email",
        "username",
        "uuid",
        "last_login",
    )
    list_display = (
        "id",
        "email",
        "first_name",
        "last_name",
        "date_joined",
    )
    list_display_links = ("email", "id")
