from rest_framework.generics import GenericAPIView


class NoPermissionNoAuthenticationView(GenericAPIView):
    permission_classes = []
    authentication_classes = []
