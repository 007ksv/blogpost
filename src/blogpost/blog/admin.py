from django.contrib import admin
from django.contrib.admin import ModelAdmin

from blogpost.blog.models import Post


@admin.register(Post)
class PostAdmin(ModelAdmin):
    readonly_fields = ["uuid", "created_at"]
