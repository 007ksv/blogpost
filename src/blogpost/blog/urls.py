from django.urls import path

from blogpost.blog.views.post import PostCreateView, PostDetailView

urlpatterns = [
    path("<int:id>/", PostDetailView.as_view(), name="post-detail"),
    path("", PostCreateView.as_view(), name="create-post"),
]
