from django.db.models import CASCADE, ForeignKey, TextField

from blogpost.apis.mixins.models import CommonModel


class Post(CommonModel):
    title = TextField(blank=True, default="")
    body = TextField(blank=True, default="")
    user = ForeignKey("apis.User", on_delete=CASCADE)

    class Meta:
        db_table = "blogpost_post"
        verbose_name = "Post"
        verbose_name_plural = "Posts"

    def __str__(self):
        return f"{self.__class__.__name__}({self.pk}) : User({self.user_id})"
