from rest_framework import filters
from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)

from blogpost.apis.permission import IsOwnerOrReadOnly
from blogpost.apis.utils.response_handler import (
    create_response,
    safe_view_request_handler,
)
from blogpost.blog.models import Post
from blogpost.blog.serializers.post import PostCreateUpdateSerializer


class PostCreateView(ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostCreateUpdateSerializer
    filter_backends = [filters.OrderingFilter]
    ordering = ["-id"]

    @safe_view_request_handler()
    def get(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data = create_response(True, item=response.data)
        return response

    @safe_view_request_handler()
    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        response.data = create_response(True, item=response.data)
        return response


class PostDetailView(RetrieveUpdateDestroyAPIView):
    queryset = Post.objects
    permission_classes = [
        IsOwnerOrReadOnly,
    ]
    serializer_class = PostCreateUpdateSerializer
    lookup_field = "id"

    @safe_view_request_handler()
    def get(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        response.data = create_response(True, item=response.data)
        return response

    @safe_view_request_handler()
    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        response.data = create_response(True, item=response.data)
        return response

    @safe_view_request_handler()
    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        response.data = create_response(True, item=response.data)
        return response
