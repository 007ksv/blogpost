from rest_framework import serializers

from blogpost.apis.serializers import (
    UserDetailSerializer as PostUserSerializer,
)
from blogpost.blog.models import Post


class PostListSerializer(serializers.ModelSerializer):
    user = PostUserSerializer()

    class Meta:
        fields = [
            "id",
            "uuid",
            "title",
            "body",
            "created_at",
            "user",
        ]
        model = Post


class PostCreateUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        fields = [
            "title",
            "body",
        ]
        model = Post

    def create(self, validated_data):
        user = self.context.get("request").user
        validated_data["user"] = user
        post = super().create(validated_data)
        return post

    def to_representation(self, instance):
        return PostListSerializer(instance).data
