from django.template.loader import render_to_string

from blogpost.apis.task.email import send_notification


class BaseNotification:
    """
    Base class for all notification related work
    """

    # email subject
    email_subject_template = None

    # email html template
    email_body_template = None

    def __init__(self, context=None):
        self.context = context or {}

    def send_mail(self, to):
        subject = self.email_subject_template.format(**self.context)
        body = render_to_string(self.email_body_template, context=self.context)
        send_notification.delay(subject=subject, body=body, to=to)
