# blogpost Backend
A backend API project of blogpost using [django rest framework][1]
***

#### Requirements
1. Python 3.6.9
2. virtualenv
3. poetry

***


#### Installation Steps
1. Clone repo to system using 
    ```
    $ git clone https://gitlab.com/007ksv/blogpost.git
    ``` 
    and goto project root.
2. Create a virtualenv of name **.venv** using
    ```
    $ virtualenv -p python3.6 .venv
    ```
3. Activate the newly created virtualenv
    ```
    $ source .venv/bin/activate
    ```
    or
    ```
    $ poetry shell
    ```
4. Install all the python dependencies related to project.
    ```
    $ poetry install
    ```
5. To add new python package dependency
   ```
   $ poetry add dependency_name

   # if dependency is for development purpose only
   $ poetry add -D dependency_name
   ```

6. Also, update the `requirements.txt` files
    ```
    $ poetry export -f requirements.txt --without-hashes -o requirements.txt
    
    $ poetry export -f requirements.txt --without-hashes --dev -o requirements-dev.txt
    ```

[1]: https://www.django-rest-framework.org/